<?php
include "header1.php";
include "koneksi.php";
session_start();
$tm_profil=mysqli_query($konek,"select * from data_siswa where
nomor_induk='".@$_SESSION['nomor_induk']."'");
$data_tm=mysqli_fetch_array($tm_profil);
if(!$_SESSION['login_user']){
	header ('location:home.php');
}
?>
<div class="frame_popup" id ="popup">
<div class="b">
<h2>Profile</h2>
<form action="proses_ubah.php" method="post" enctype="multipart/form-data">
<table align="center">
	<tr>
		<td>nomor induk</td><td><input type="text" name="nomor_induk" value="<?php echo $data_tm['nomor_induk'];?>"></td>
	</tr>
	<tr>
		<td>nama siswa</td><td><input type="text" name="nama_siswa" value="<?php echo $data_tm['nama_siswa'];?>"></td>
	</tr>
	<tr>
		<td>tempat tinggal</td><td><input type="text" name="tempat_tinggal" value="<?php echo $data_tm['tempat_tinggal'];?>"></td>
	</tr>
	<tr>
		<td>telp</td><td><input type="text" name="telp" value="<?php echo $data_tm['telp'];?>"></td>
	</tr>
	<tr>
		<td>Agama</td><td>
		<?php 
		$arr_agama=array("islam"=>"islam","kristen"=>"kristen","katolik"=>"katolik","hindu"=>"hindu","budha"=>"budha");
		?>
		<select name="agama">
		<option></option>
		<?php 
		foreach($arr_agama as $ag){
			if($ag==$data_tm['agama']){
				$selek="selected";
			} else {
				$selek="";
			}
			echo "<option value='$ag' $selek>$ag</option>";
		}
		?>
		</select>
		</td>
	</tr>
	
	<tr>
		<td>gender</td><td>
		<?php
		$gender=array("L"=>"Laki-laki","P"=>"Perempuan");
		?>
		<select name="gender">
		<option></option>
		<?php
		foreach($gender as $kunci=>$nilai){
			if($kunci==$data_tm['gender']){
				$selek="selected";
			} else {
				$selek="";
			}
			echo "<option value='$kunci' $selek>$nilai</option>";
		}
		?>
		</select>
		</td>
	</tr>
	<tr>
		<td>tempat_lahir</td><td><input type="text" name="tempat_lahir" value="<?php echo $data_tm['tempat_lahir'];?>"></td>
	</tr>
	<tr>
		<td>tanggal_lahir</td><td><input type="date" name="tanggal_lahir" value="<?php echo $data_tm['tanggal_lahir'];?>"></td>
	</tr>
	<tr>
		<td>Username</td><td><input type="text" name="username_siswa" value="<?php echo $data_tm['username_siswa'];?>"></td>
	</tr>
	<tr>
		<td>Password</td><td><input type="password" name="password" value="<?php echo $data_tm['password'];?>"></td>
	</tr>
</table>
<input type="submit" name="ubah" value="Ubah Profil">
</form>
</div>
</div>
<?php
include "footer1.php";
?>
